import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home/home-page/home-page.component';
import { QuizPageComponent } from './quiz/quiz-page/quiz-page.component';
import { VocabularyPageComponent } from './vocabulary/vocabulary-page/vocabulary-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent, pathMatch: 'full', data: {animation: 'HomePage'} },
  { path: 'kviz', component: QuizPageComponent, pathMatch: 'full', data: {animation: 'QuizPage'} },
  { path: 'slovicka', component: VocabularyPageComponent, pathMatch: 'full', data: {animation: 'VocabularyPage'} },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
