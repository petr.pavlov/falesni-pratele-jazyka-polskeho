export interface PageInfoType {
  title: string,
  description: string
}