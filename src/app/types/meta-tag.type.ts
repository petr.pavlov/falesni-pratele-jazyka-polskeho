export interface MetaTagType {
  name: string;
  content: string;
}