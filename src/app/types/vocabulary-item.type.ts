export interface VocabularyItemType {
  cs: string;
  id: string,
  opt: Array<string>;
  pl: string;
  order?: number;
}