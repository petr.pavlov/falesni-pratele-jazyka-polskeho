import { VocabularyItemType } from "./vocabulary-item.type";

export interface AnswerType {
  isCorrect: boolean;
  quizItem: VocabularyItemType;
}