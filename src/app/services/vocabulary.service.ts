import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { tap } from 'rxjs/operators';
import { VocabularyItemType } from '../types/vocabulary-item.type';

@Injectable({
  providedIn: 'root'
})
export class VocabularyService {

  constructor(private store: AngularFirestore) { }

  getVocabulary(){
    return this.store.collection<VocabularyItemType>('vocabulary').valueChanges({ idField: 'id' })
      .pipe(tap(result => result.sort()));
  }
}
