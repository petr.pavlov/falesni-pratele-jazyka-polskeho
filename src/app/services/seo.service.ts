import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

import { PageInfoType } from '../types/page-info.type';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(private title: Title, private meta: Meta) { }

  updateTags(pageInfo: PageInfoType): void {
    this.title.setTitle(pageInfo.title);
    this.meta.updateTag({ name: 'description', content: pageInfo.description });
    this.meta.updateTag({ property: 'og:title', content: pageInfo.title });
    this.meta.updateTag({ property: 'og:description', content: pageInfo.description });
  }
}
