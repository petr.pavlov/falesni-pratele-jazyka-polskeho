import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home/home-page/home-page.component';
import { PageLayoutComponent } from './shared-components/page-layout/page-layout.component';
import { FooterComponent } from './shared-components/footer/footer.component';
import { SubpageLayoutComponent } from './shared-components/subpage-layout/subpage-layout.component';
import { HeaderComponent } from './shared-components/header/header.component';
import { QuizPageComponent } from './quiz/quiz-page/quiz-page.component';
import { VocabularyPageComponent } from './vocabulary/vocabulary-page/vocabulary-page.component';
import { BoardComponent } from './shared-components/board/board.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { QuizComponent } from './quiz/quiz/quiz.component';
import { QuizQuestionComponent } from './quiz/quiz-question/quiz-question.component';
import { QuizResultComponent } from './quiz/quiz-result/quiz-result.component';
import { SpinnerComponent } from './shared-components/spinner/spinner.component';
import { VocabularyComponent } from './vocabulary/vocabulary/vocabulary.component';
import { AudioComponent } from './shared-components/audio/audio.component';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    PageLayoutComponent,
    FooterComponent,
    SubpageLayoutComponent,
    HeaderComponent,
    QuizPageComponent,
    VocabularyPageComponent,
    BoardComponent,
    QuizComponent,
    QuizQuestionComponent,
    QuizResultComponent,
    SpinnerComponent,
    VocabularyComponent,
    AudioComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAnalyticsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
