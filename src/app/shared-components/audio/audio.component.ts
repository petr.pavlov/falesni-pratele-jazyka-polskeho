import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.scss']
})
export class AudioComponent implements OnInit {

  @Input() fileName: string;

  @ViewChild('audioPlayer') audioPlayerRef: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  onAudioPlay(){
    this.audioPlayerRef.nativeElement.play();
  }

}
