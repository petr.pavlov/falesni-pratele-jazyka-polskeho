import { Component, OnInit } from '@angular/core';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  preview = '';
  pageInfo = {
    title: 'Úvod | Falešní přátelé jazyka polského',
    description: 'Vyzkoušejte svojí znalost polštiny nebo se jen podívejte na slovíčka, která znějí v češtině stejně, ale mají jiný význam.'
  }

  constructor(private seoService: SeoService) { }

  ngOnInit(): void {
    this.seoService.updateTags(this.pageInfo);
  }

}
