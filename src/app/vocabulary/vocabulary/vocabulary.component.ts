import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { VocabularyService } from 'src/app/services/vocabulary.service';
import { VocabularyItemType } from 'src/app/types/vocabulary-item.type';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-vocabulary',
  templateUrl: './vocabulary.component.html',
  styleUrls: ['./vocabulary.component.scss']
})
export class VocabularyComponent implements OnInit, OnDestroy {

  itemsLoading = true;
  vocabularySubscription: Subscription;
  $vocabulary: Observable<VocabularyItemType[]>;
  vocabularySorted: VocabularyItemType[] = [];
  appUrl: string;

  constructor(vocabularyService: VocabularyService) {
    this.$vocabulary = vocabularyService.getVocabulary();
    this.vocabularySubscription = this.$vocabulary.subscribe(items => {
      this.itemsLoading = false;
      this.vocabularySorted = items.sort((a, b) => {
        return a.pl.localeCompare(b.pl)
      });
    });
  }

  ngOnInit(): void {
    this.appUrl = environment.url;
  }

  ngOnDestroy(): void {
    this.vocabularySubscription.unsubscribe();
  }

}
