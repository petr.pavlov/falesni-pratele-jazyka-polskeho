import { Component, OnInit } from '@angular/core';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-vocabulary-page',
  templateUrl: './vocabulary-page.component.html',
  styleUrls: ['./vocabulary-page.component.scss']
})
export class VocabularyPageComponent implements OnInit {

  preview = '';
  pageInfo = {
    title: 'Slovíčka | Falešní přátelé jazyka polského',
    description: 'Projděte si přehled slovíček, která znějí podobně v češitně i polštině, ale mají jiný význam.'
  }

  constructor(private seoService: SeoService) { }

  ngOnInit(): void {
    this.seoService.updateTags(this.pageInfo);
  }

}
