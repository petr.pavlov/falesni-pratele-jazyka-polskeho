import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AnswerType } from '../../types/answer.type';
import { VocabularyItemType } from '../../types/vocabulary-item.type';

@Component({
  selector: 'app-quiz-question',
  templateUrl: './quiz-question.component.html',
  styleUrls: ['./quiz-question.component.scss']
})
export class QuizQuestionComponent implements OnInit {

  alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'd'];
  options: Array<string>;
  correct = false;
  showResult = false;

  @Input() question: VocabularyItemType;
  @Input() order: number;
  @Output() nextQuestion = new EventEmitter();
  @ViewChild("nextbutton") btnNextEl: ElementRef;

  constructor() { }

  ngOnInit(): void {
    this.options = this.question.opt.slice();
    this.options.push(this.question.cs);
    this.options.sort();
  }

  onOptionClick(option: string) {
    if(option == this.question.cs) {
      this.correct = true;
    }
    this.showResult = true;
    this.btnNextEl.nativeElement.focus();
  }

  onNextQuestionClick() {
    this.nextQuestion.emit(<AnswerType>{isCorrect: this.correct, quizItem: this.question});
  }
}
