import { Component, OnInit } from '@angular/core';
import { SeoService } from 'src/app/services/seo.service';
import { PageInfoType } from 'src/app/types/page-info.type';

@Component({
  selector: 'app-quiz-page',
  templateUrl: './quiz-page.component.html',
  styleUrls: ['./quiz-page.component.scss']
})
export class QuizPageComponent implements OnInit {

  pageInfo: PageInfoType = {
    title: 'Kvíz | Falešní přátelé jazyka polského',
    description: 'Jednoduchý kvíz, který prověří vaší znalost záludných slovíček polského jazyka.'
  }

  constructor(private seoService: SeoService) { }

  ngOnInit(): void {
    this.seoService.updateTags(this.pageInfo);
  }

}
