import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { VocabularyItemType } from '../../types/vocabulary-item.type';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnswerType } from '../../types/answer.type';
import { VocabularyService } from 'src/app/services/vocabulary.service';
import { UtilityService as utilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit, OnDestroy {

  quizStep = 1;
  showResult = false;
  itemsLoading = true;
  correctAnswers = 0;
  itemsCount: number;
  quizScore: number;

  quizItemsSubscription: Subscription;
  $quizItems: Observable<VocabularyItemType[]>;
  quizItems: any;

  constructor(vocabularyService: VocabularyService) {
    this.$quizItems = vocabularyService.getVocabulary();
    this.quizItemsSubscription = this.$quizItems
      .subscribe(items => {
        this.itemsLoading = false;
        this.itemsCount = items.length;
        this.quizItems = utilityService.shuffleArray(items);
      });
  }

  ngOnInit(): void {

  }

  onNextQuestionRequest(answer: AnswerType) {
    if(answer.isCorrect) {
      this.correctAnswers++;
    }

    this.quizStep += 1;

    if(this.quizStep > this.itemsCount) {
      this.showResult = true;
      this.quizScore = Math.ceil(this.correctAnswers / this.itemsCount * 100);
    }
  }

  ngOnDestroy(): void {
    this.quizItemsSubscription.unsubscribe();
  }

}
