import { Component, Input, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-quiz-result',
  templateUrl: './quiz-result.component.html',
  styleUrls: ['./quiz-result.component.scss']
})
export class QuizResultComponent implements OnInit {

  @Input() score: number;
  scoreText: string;
  appUrl: string;

  constructor() { }

  ngOnInit(): void {
    this.scoreText = this.getVerbalScore();
    this.appUrl = environment.url;
  }

  onRepeatClick(): void {
    window.location.reload();
  }

  getVerbalScore(): string {
    if(this.score < 20){
      return 'Nie dobrze. Lepiej nikomu nie mów.';
    }
    if(this.score < 40){
      return 'Słabiutko. Lepiej zostań w Czechach.';
    }
    if(this.score < 60){
      return 'Całkiem spoko, ale raczej spróbuj jeszcze raz.';
    }
    if(this.score < 80){
      return 'Bardzo dobrze. Troche lepiej i bym myślał, że jesteś Polakiem.';
    }

    return 'Zaskakujące jak  dobrze umiesz mówić po polsku. Nie jesteś przypadkiem Polakiem?!';
  }
}
