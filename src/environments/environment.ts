// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: "fpjp.fun",
  firebase : {
    apiKey: "AIzaSyDJ4GmwjoyMFK4FQSw6eL9A5MCjHE9zg5c",
    authDomain: "fpjp-6e007.firebaseapp.com",
    projectId: "fpjp-6e007",
    storageBucket: "fpjp-6e007.appspot.com",
    messagingSenderId: "408345757770",
    appId: "1:408345757770:web:3d0c5a583bc4c4d817beb3",
    measurementId: "G-53Z6PH02MP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
