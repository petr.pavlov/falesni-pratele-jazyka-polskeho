# Environments

## Firebase

`https://fpjp-6e007.firebaseapp.com/`

### Build
`ng build`

### Deploy
`firebase deploy`

## Google app for SSR (production)
`https://fpjp-6e007.web.app/`

### Build
`npm run build:ssr`

### Deploy
`gcloud app deploy`

## Production
`https://fpjp.fun`